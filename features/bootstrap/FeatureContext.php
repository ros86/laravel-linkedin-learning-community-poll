<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->bearerToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjYzNDllMzQ1Mjg3Mzg2NWE4NGY2ZWM1YmJjZjBhNzFhNGU4Y2ExZmE4YzQzNzc2YzViZmI1ZmVmN2Y4NTA5OTUxMzk3M2M2YTE5ZDExOGUyIn0.eyJhdWQiOiIzIiwianRpIjoiNjM0OWUzNDUyODczODY1YTg0ZjZlYzViYmNmMGE3MWE0ZThjYTFmYThjNDM3NzZjNWJmYjVmZWY3Zjg1MDk5NTEzOTczYzZhMTlkMTE4ZTIiLCJpYXQiOjE1NDcxNjM0MjQsIm5iZiI6MTU0NzE2MzQyNCwiZXhwIjoxNTc4Njk5NDIzLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.kIaG5a5C0aJdMaclc0H5aMN3cCkF-I4_5ZFkdpUinAKWA7O_lFdomc13cFZGrVi12frJRw6_EGSiut2yD0h9HoM8AVWFmyObTaH5lNtQa95BCpkkK44h2pd6-C3lyBMxqmrNPTu4Yxz6wMLnqzIdtRk4tNCmQH5TZ30nFnKthnAq00HYScQ7ppor8zKZl_Denv2O9P8SUJQESDU6PcsmwLiZPIf0EZs-Fc8e1LUb_ppqlqb6cJLD-AWVpKMl5vO7L49plxJv4ymQE8AW6HZFVikuR36IVZztD08nwcoXyw03z_WWVtQ1ReFAXuiYzCxHxdrYbiAiM98dPMHueRcL4Ui-00CrQUee2MgwQmnvFF9iAAhaDjFKLtT2O3Gy1-zA_THz_tf_MAay_OMF9CynYfE-uWYmz2GJeqjHaEsnnzFUtMTEUJjeRc8fOVAzSHf8IJKE5dA1UscdLycgXeJQsbdBX6yB7cgraDl5U-wFATA81-2cA7zk3HsiWdi9ZbmtlI1aqM1STH5L39L9oUMCnS9e3WUiKXbrjSFAjsJUWDz-snOsgqLemBMe5Z8gZQ5u3TMPK8xAsDI9hTuqe8Dwja1_cjmLb3nri7M7i9vyeFlk9uQgTb0MNkacVPQVdA7hP7fNNnV6NoclSORh-lUpdyvRD5tVWR6yuYWInE9N1QI";
    }

    /**
     * @Given I have the payload:
     */
    public function iHaveThePayload(PyStringNode $string)
    {
        $this->payload = $string;
    }

    /**
     * @When /^I request "(GET|PUT|POST|DELETE|PATCH) ([^"]*)"$/
     */
    public function iRequest($httpMethod, $argument1)
    {
        $client = new GuzzleHttp\Client();
        $this->response = $client->request(
            $httpMethod,
            'http://127.0.0.1:8000' . $argument1,
            [
                'body' => $this->payload,
                'headers' => [
                    "Authorization" => "Bearer {$this->bearerToken}",
                    "Content-Type" => "application/json",
                ],
            ]
        );
        $this->responseBody = $this->response->getBody(true);
    }

    /**
     * @Then /^I get a response$/
     */
    public function iGetAResponse()
    {
        if (empty($this->responseBody)) {
            throw new Exception('Did not get a response from the API');
        }
    }

    /**
     * @Given /^the response is JSON$/
     */
    public function theResponseIsJson()
    {
        $data = json_decode($this->responseBody);

        if (empty($data)) {
            throw new Exception("Response was not JSON\n" . $this->responseBody);
        }
    }

    /**
     * @Then the response contains :arg1 records
     */
    public function theResponseContainsRecords($arg1)
    {
        $data = json_decode($this->responseBody);
        $count = count($data);
        return ($count == $arg1);
    }

    /**
     * @Then the question contains a title of :arg1
     */
    public function theQuestionContainsATitleOf($arg1)
    {
        $data = json_decode($this->responseBody);

        if ($data->title != $arg1)
            throw new Exception("The title does not match");
    }

}